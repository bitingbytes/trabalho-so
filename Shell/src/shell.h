#pragma once

#include "common.h"
#define LINHA_TAMANHO 32

/* Variaveis */
class FProcesso
{
	char* Caminho;

	int32 Status;
	std::vector<char*> Argumentos;
	char* LinhaAtual;
	
public:
	~FProcesso(); // Destrutor

	/* Metodos */
	bool Iniciar();
	void ObterLinhaCmd(char **OutLinha);
	void Parse();
	void Update();
	void Executar(int32 *Status);
	void Fechar();

	void SetarDiretorio();
};