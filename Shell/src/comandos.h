#pragma once
#include "common.h"

CMD ("fat")
int32 CmdFatorial(const std::vector<char*>& InArgumentos)
{
	if (InArgumentos.size() > 1)
	{
		uint32 X = std::stoi(InArgumentos[1]);
		uint32 Res = 1;
		for (uint32 i = X; i > 1; --i)
		{
			Res *= i;
		}
		std::cout << Res << std::endl;
	}
	else
	{
		LOG("Informe um numero");
	}

	return 1;
}

CMD ("cd")
int32 CmdChangeDir(const std::vector<char*>& InArgumentos)
{
	if (InArgumentos.size() > 1)
	{
		if (chdir(InArgumentos[1]) != 0)
		{
			ELOG();
		}
	}
	else
	{
		LOG("Nao ha diretorio informado");
	}

	return 1;
}

CMD ("kil")
int32 CmdKilProcess(const std::vector<char*>& InArgumentos)
{
	LOG("Ate");
	exit(0);
}

CMD ("help")
int32 CmdHelp(const std::vector<char*> &InArgumentos)
{
	LOG("Uso");
	LOG("cd [dir] = muda para o diretorio [dir]");
	LOG("fat [inteiro] = fatorial do numero [inteiro]");
	LOG("kil = mata o processo");

	return 1;
}

#include "comandos.gen.h"