#pragma once

#include <stdio.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <map>
#include <vector>
#include <libgen.h>

typedef unsigned char uint8;
typedef unsigned short int uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;

typedef signed char int8;
typedef signed short int int16;
typedef signed int int32;
typedef signed long long int64;

typedef uint8 byte;

#define LOG(x) std::cout << x << std::endl
#define ELOG() perror("[ERRO]")

// Define funcoes para a Shell
#define CMD(x)

typedef int32(*TArrayFuncao)(const std::vector<char *> &InArgumentos);