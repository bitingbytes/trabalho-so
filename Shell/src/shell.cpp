#include "shell.h"
#include "comandos.h"

FProcesso::~FProcesso()
{
	Status = -1;
	Argumentos.clear();
	delete LinhaAtual;
	delete Caminho;
}

bool FProcesso::Iniciar()
{
	SetarDiretorio();
	LinhaAtual = new char[LINHA_TAMANHO];

	pid_t pid;

	pid_t wpid;

	pid = fork();

	/*
		O fork deu certo?
		Quando o fork der certo, o processo sera
		duplicado. Se o processo for o filho, ele ir� dar um execvp
		para ser trocado pelo processo que desejamos executar.
		Se ele for o pai
	*/
	if (pid < 0) // Falha no fork
	{
		ELOG();
	}
	else if (pid == 0) // Estamos no processo filho.
	{
		// Executa as coisas da shell.
		execvp(Argumentos[0], Argumentos.data());
	}
	else // Estamos no processo pai.
	{
		while (true)
		{
			wpid = waitpid(pid, &Status, WUNTRACED);
			if ((WIFSIGNALED(Status) || WIFEXITED(Status)))
				break;
		}
	}

	return true;
}

void FProcesso::Update()
{
	while (true)
	{
		std::cout << Caminho << "$ ";
		ObterLinhaCmd(&LinhaAtual);
		Parse();
		Executar(&Status);

		if (!Status)
			break;
	}
}

void FProcesso::Executar(int32 *Status)
{
	if (Argumentos.size() == 0)
	{
		LOG("Nao ha comandos.");
		exit(1);
	}

	for (auto Cmd : Comandos)
	{
		if (strcmp(Argumentos[0], Cmd.first.c_str()) == 0)
		{
			*Status = (*Cmd.second)(Argumentos);
			return;
		}
	}

	*Status = Iniciar();
}

void FProcesso::Fechar()
{
	this->~FProcesso();
}

void FProcesso::SetarDiretorio()
{
	Caminho = getcwd(nullptr, 0);
}

void FProcesso::ObterLinhaCmd(char **OutLinha)
{
	char *TempLinha = NULL;
	size_t bufsize = 0;

	if (getline(&TempLinha, &bufsize, stdin) == -1)
	{
		ELOG();
		exit(1);
	}

	if (TempLinha == 0 || strlen(TempLinha) <= 1)
	{
		LOG("Nao ha comandos");
		exit(1);
	}

	*OutLinha = TempLinha;
}

void FProcesso::Parse()
{
	char *TempToken;
	Argumentos.clear();

	TempToken = strtok(LinhaAtual, " \n\b\t\r");
	while (TempToken != NULL)
	{
		Argumentos.push_back(TempToken);
		TempToken = strtok(0, " \n\b\t\r");
	}

	Argumentos.insert(Argumentos.end(), '\0');
}
