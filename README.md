# Uma Shell qualquer
Trabalho para a disciplina Sistemas Operacionais da Universidade Federal de Uberlândia, Faculdade de Engenharia da Computação.

# O que é uma Shell
Shell é um programa que interpreta linhas de comando.
O usuário entra com um comando, normalmente em texto, e a shell fica a cargo de dizer como executar-lo.

# Ciclo de vida
O processo de execução de uma shell é dado na seguinte ordem:

1. <b>Inicialização:</b> O programa inicializa seu ambiente, pode ser pela leitura de um arquivo ou pela inicialização de suas variaveis.
2. <b>Interpretação:</b>
A shell lê e executa comandos que podem ser passados tanto diretamente atravez de um terminal ou atraves de um arquivo.
3. <b>Finalização:</b> A shell libera a memoria que foi usada por ela para sua execução e é fechada.

# Iniciando um Processo
- <b>fork():</b> Duplica o processo e executa ambos. O original é chamado de pai e a duplicata é chamada de filho.
- <b>exec():</b> Substitui o programa em execução por um novo, que é o programa desejado para ser executado. Há algumas variações como o execvp, que indica que o exec receberá um vector pointer ao invez de apenas um argumento.
- <b>waitpid:</b> Espera que o filho do processo dono da chamada mude de estado. 

# Minha Shell
## Build
- Para compilar os binários:
```
$ mkdir build
$ cd build
$ cmake ..
$ make
```
- Para executar a Shell:

```
./shell.sh
```

## Comandos
### cd [arg]
Muda do diretório atual para o informado no <i>arg</i>.
### fat [arg]
Calcula o fatorial de <i>arg</i>.
### kil
Mata o processo, ou seja, fecha o programa.

## Criando Comandos
### HeaderTool
Refatora os header files que contenham a macro ```CMD()```
### Como usar
Para criar uma função que poderá ser chamada por um comando na shell deve se seguir o exemplo:
```c++
CMD ("comando")
int NomeFuncao(const std::vector<char*> &InArgumentos)
{
	// Corpo.
}
```
O macro CMD recebe o comando que irá chamar a função abaixo dela.