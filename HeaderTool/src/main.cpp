#include <iostream>
#include <string.h>
#include <fstream>

#define LOG(x) std::cout << x << std::endl

int main(int argc, char** argv)
{
	LOG("Iniciando Header Tool");
	char* Path;
	size_t PathLength;

	if (argc == 1)
	{
		LOG("Kd o path?");
		return 1;
	}
	else
	{
		Path = argv[1];
	}

	PathLength = strlen(Path);

	if (Path[PathLength - 2] == '.' && Path[PathLength - 1] == 'h')
	{
		std::ifstream HeaderFile;
		std::ofstream GeneratedFile;
		HeaderFile.open(Path, std::ios::in);

		LOG("Abrindo arquivo");
		LOG(Path);

		std::string GenPath = Path;
		GeneratedFile.open(GenPath.replace(GenPath.end() - 2, GenPath.end(), ".gen.h").c_str(), std::ios::out);
		LOG("Criando arquivo");
		LOG(GenPath.c_str());

		if (HeaderFile.is_open() && GeneratedFile.is_open())
		{
			LOG("Arquivo aberto");
			LOG("Arquivo criado");
			char* CurrentCharacter = new char[64];

			GeneratedFile << "#pragma once\n\n";
			GeneratedFile << "std::map<std::string, TArrayFuncao> Comandos =\n{\n";

			LOG("Base Criada");

			while (HeaderFile >> CurrentCharacter)
			{
				std::string CmdString = CurrentCharacter;

				if (strcmp(CmdString.c_str(), "CMD") == 0)
				{
					HeaderFile >> CurrentCharacter;

					if (GeneratedFile.tellp() > 64)
					{
						GeneratedFile << ",{";
					}
					else
					{
						GeneratedFile << "{";
					}

					std::string CurString = CurrentCharacter;
					CurString.erase(CurString.begin());
					CurString.erase(CurString.end() - 1);
					GeneratedFile << CurString.c_str() << ", &";

					std::cout << CurString.c_str() << std::endl;

					HeaderFile >> CurrentCharacter;
					HeaderFile >> CurrentCharacter;

					CurString.empty();
					CurString = CurrentCharacter;

					GeneratedFile << CurString.substr(0, CurString.find("(")).c_str();

					GeneratedFile << "}\n";
				}
			}
			GeneratedFile << "};\n\n";
		}
		HeaderFile.close();
		GeneratedFile.close();
		LOG("Codigo refatorado com sucesso.");
	}
	else
	{
		std::cout << "Caminho: " << Path << std::endl;
		std::cout << "Caminho deve ser da extensao .h e nao " << Path[PathLength - 2] << Path[PathLength - 1] << std::endl;
		return 1;
	}

	return 0;
}